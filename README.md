# Prática de programação básica em C

Faça os seguintes exercícios. Só serão aceitos os exercicios implementados e versionados em repositório GIT (crie um repositório unico para seus exercicios e separe cada exercicio em um diretório especifico)

1) Faça um algoritmo em C,usando modularização, que
Recebe o valor de dois lados de um sala

- Calcula a área da sala

- Imprime o resultado da area

- Imprime se a área é superior a 10m2

2) 

Faça um algoritmo em C, usando modularização, que calcule a média parcial da disciplina de Introdução à Computação

- Recebe o valor das notas, conforme definido no Plano de aula

- Calcula a média final do aluno

- Imprime o resultado.

- Se aprovado, imprime uma saudação e encerra.

- Se não aprovada, recebe a nota da prova final

- Calcula a média final

- Imprime se o aluno foi aprovado ou reprovado.
