#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#define ARQ_IN "commands.dat" //nome do arquivo de entrada
#define ARQ_OUT "regcommands.txt" //nome do arquivo de saída
#define MAXLINHA 8 //quantidade máxima de comandos e códigos.. Caso adicione mais comandos, altere aqui.
#define MAXLENGTH 15 //quantidade máxima de caracteres.


void clearScreen()
{
      const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J~$";
      write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

float calculaCosseno(char *input, char *cmd)
{
char cpcmd[MAXLENGTH], cpinput[MAXLENGTH];
int i;
float numerador, denominador1, denominador2, cosseno;
}

//inicialização de variáveis auxiliares para calculo do cosseno
numerador=0;
denominador1=0;
denominador2=0;

//inicialização com zeros os dois vetores de calculo da formula
memset(cpcmd, '\0', MAXLENGTH);
memset(cpinput, '\0', MAXLENGTH);

strcpy(cpcmd, cmd);
strcpy(cpinput, input);


for(i=0; i<MAXLENGTH; i++)
{
numerador += cpinput[i]*cpcmd[i];
denominador1 += pow(cpinput[i],2);
denominador2 += pow(cpcmd[i],2);

cosseno = numerador / ( sqrt(denominador1) * sqrt(denominador2));
return cosseno;

}


int main()
{


FILE *arquivo_in, *arquivo_out;

char comando[MAXLINHA][MAXLENGTH], input[MAXLENGTH], linha; char copycomando[MAXLENGTH], copyinput[MAXLENGTH];
int confirma, i, j, codigo[MAXLINHA];
float cosseno[MAXLINHA];

arquivo_in = fopen(ARQ_IN, "r");
arquivo_out= fopen(ARQ_OUT, "w");


//carregar comandos
j=0;
linha = fscanf(arquivo_in, "%s %X", comando[j], &codigo[j]);
while(linha != EOF)
{
j++;
linha = scanf(arquivo_in, "%s %X", comando[j], &codigo[j]);
}


    clearScreen();


//Processo continuo
    while(1)
    {
 scanf("%s", input);
                
        for(i=0; i<MAXLINHA; i++)
        {
        cosseno [i] = calculaCosseno(input, comando[i]);
        printf("comando %d é %.1f\n", i, cosseno[i]*100 );
        }

    }

}
